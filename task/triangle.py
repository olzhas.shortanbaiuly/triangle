def is_triangle(a, b, c):
    # start with checking lengths to satisfy the nonzero area condition
    if a <= 0 and b <= 0 and c <= 0:
        return False
    
    #check the triangle inequality theorem stating that sum of two sides should always be greater than the other side
    return ((a+b) > c) and ((a+c) > b) and ((b+c) > a)
